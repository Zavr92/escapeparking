﻿using UnityEngine;


public class MovableObject : MonoBehaviour
{

    public uint moveDirection = 0;
    private bool isPressed = false;
    private BoxCollider2D collider;

    private void Awake()
    {
        this.collider = GetComponent<BoxCollider2D>();
        this.transform.TransformPoint(this.collider.bounds.center);
    }

 
    void OnMouseUp()
    {
        this.isPressed = false;
        print("MouseUP");

    }

    void OnMouseDown()
    {
        this.isPressed = true;
        print("MouseDown");
    }

   
    void FixedUpdate()
    {
        Vector2 mousePosition = this.GetMousePosition();
        
        if (this.isPressed)
        {
          

            if (this.HasMove(mousePosition))
            {
            
                this.MoveObject(mousePosition);
               
            }
        }
        
    }

    Vector2 GetDirection( Vector2 newPosition)
    {
        Vector2 oldPosition = this.transform.position;
        Vector2 delta = oldPosition - newPosition;
        //print(delta.y);

        if(delta.x < 0 && this.moveDirection == 1)
        {
            //Debug.Log("Direction right");
            return Vector2.right;
        }
        else if (delta.x > 0 && this.moveDirection == 1)
        {
            //Debug.Log("Direction left");
            return Vector2.left;
        }
        else if (delta.y < 0 && this.moveDirection == 0)
        {
            //Debug.Log("Direction up");
            return Vector2.up;
        }
        else if (delta.y > 0 && this.moveDirection == 0)
        {
            //Debug.Log("Direction down");
            return Vector2.down;
        }
        else
        {
            throw new System.Exception();
        }
    }

    void MoveObject(Vector2 position)
    {
        if(this.moveDirection == 0)
        {
            this.transform.position = new Vector3(this.transform.position.x, position.y, 0);
        }
        else
        {
            this.transform.position = new Vector3(position.x, this.transform.position.y, 0);
        }
        
    }

    bool HasMove(Vector2 newPosition)
    {
        Vector2 oldPosition = this.transform.position;
        Vector2 direction = this.GetDirection(newPosition);

        Vector2 rayStart;
        Vector2 rayEnd;

        if(direction == Vector2.up)
        {
            rayStart = oldPosition + new Vector2(0, this.collider.size.y / 2 + 0.01f);
            rayEnd = rayStart + new Vector2(0, 1);
            
        }
        else if(direction == Vector2.down)
        {
            rayStart = oldPosition - new Vector2(0, this.collider.size.y / 2 + 0.01f);
            rayEnd = rayStart - new Vector2(0, 1);
        }
        else if(direction == Vector2.left)
        {
            rayStart = oldPosition - new Vector2(this.collider.size.x / 2 + 0.01f, 0);
            rayEnd = rayStart - new Vector2(1, 0);
        }
        else if(direction == Vector2.right)
        {
            rayStart = oldPosition + new Vector2(this.collider.size.x / 2 + 0.01f, 0);
            rayEnd = rayStart + new Vector2(1, 0);

        }
        else
        {
            throw new System.Exception();
        }

        RaycastHit2D rayHit = Physics2D.Linecast(rayStart, rayEnd);

        if (rayHit.collider)
        {
            print(rayHit.collider.name);
        }
        
        return (rayHit.collider == null);
    }

    Vector2 GetMousePosition()
    {
        Vector2 screenPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
        return worldPosition;
    }
}
