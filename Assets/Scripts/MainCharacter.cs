﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCharacter : MonoBehaviour
{
    public Rigidbody2D rb;
    public Vector2 force;
    void Start()
    {
        rb.AddForce(force);
    }
    void Update()
    {
      
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "wall")
        {
            print("столкновение сработало");
            if (transform.position.x < 0)
            {
                rb.AddForce(force);
                print("Сработал форс на x<0");
            }
            if (transform.position.x > 0)
            {
                rb.AddForce(-force);
            }
        }
    }
   
}
