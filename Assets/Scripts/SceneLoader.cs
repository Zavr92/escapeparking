﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //добавлено для загрузки сцен

public class SceneLoader : MonoBehaviour
{
    private Animator anim;
    private int levelIn;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void FadeToLevel(int level)
    {
         levelIn = level;
         anim.SetTrigger("fade");
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelIn);
    }

}
