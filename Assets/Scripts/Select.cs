﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Select : MonoBehaviour, IPointerEnterHandler , IPointerDownHandler, IPointerExitHandler
{
    public Color defaultColor;
    Vector2 defaultSise;
    public float scale =1.2f;
    public bool isChangeColor = true;
    private AudioSource clickOn;
    void Start()
    {
        defaultSise = transform.localScale;
        clickOn = GetComponent<AudioSource>();
    }
    public void OnPointerDown(PointerEventData eventData) // если таппают по элементу то...
    {
        clickOn.Play();   // плей саунд
    }
    public void OnPointerEnter(PointerEventData eventData) //меняем параметры кнопки при наведении мышки
    {
        if (isChangeColor)
        {
            GetComponent<Image>().color = Color.cyan;
        }
        GetComponent<Image>().gameObject.transform.localScale = defaultSise * scale;
    }
    public void OnPointerExit(PointerEventData eventData) //меняем параметры кнопки при отводки мышки
    {
        GetComponent<Image>().color = Color.white;
        GetComponent<Image>().gameObject.transform.localScale = defaultSise;
    }
    void Update()
    {
        
    }
}
